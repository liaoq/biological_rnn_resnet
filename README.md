# Recurrent ResNet

ImageNet Pretrained Model for ResNet and Recurrent ResNet (ResNet with Weight Sharing Across Layers)

**Reference:**

Liao, Q., & Poggio, T. (2016). Bridging the gaps between residual learning, recurrent neural networks and visual cortex. arXiv preprint arXiv:1604.03640.

**Acknowledgement:**

This material is based upon work supported by the Center for Minds, Brains and Machines (CBMM), funded by NSF STC award CCF-1231216, and part by C-BRIC, one of six centers in JUMP, a Semiconductor Research Corporation (SRC) program sponsored by DARPA.

