% I use a different version of +dagnn from MatConvNet (attached in this folder), so please remove paths to the MatConvNet's dagnn 
restoredefaultpath

model = load('my_resnet/net-epoch-12.mat');
model_shared = load('my_resnet_shared/net-epoch-12.mat'); 

%% require 3 mex files from MatConvNet. Should not require other things from MatConvNet 
% please addpath to the following mex files    
% vl_nnbnorm
% vl_nnconv
% vl_nnpool


%net = dagnn.DagNN.loadobj(model.net); %  the original version
net = dagnn.DagNN.loadobj(model_shared.net); %  the version with shared weights

% [numParam decompose] = urnet_get_num_param(net) % to get the number of parameters 

net.layers(end-2:end) = [];
%net.rebuild % for some reason, I cannot rebuild, it breaks something so the features become different

%% process multiple images in a batch, just put images in a cell array
im = {};
im{1} = imread('peppers.png'); 
im{2} = imread('peppers.png'); 

for i = 1:numel(im)
    im_ = im{i};
    % preprocess
    im_ = single(im_) ; % note: 255 range
    im_ = imresize(im_, net.meta.normalization.imageSize(1:2)) ;
    if numel( net.meta.normalization.averageImage) == 3
        im_ = bsxfun(@minus, im_ , permute( net.meta.normalization.averageImage(:), [3 2 1]) ) ;
    else
        im_ =  im_ -  net.meta.normalization.averageImage;
    end
    im{i} = im_;
end
im = cat(4,im{:});

net.conserveMemory = 0;
net.mode='test'; % please use test mode
%net.mode = 'normal';
useGPU = 1;
if useGPU
    net.move('gpu') % may use GPU
    im = gpuArray(im);
end

try 
    net.eval({net.vars(1).name, im}) ;
catch err
    disp(err.message);
end
  
% all the features are in net.vars
net.vars

% show the classification result
scores = gather(net.vars(end-4).value) ; 
[~, sorted_idx] = sort( squeeze( scores(:,:,:,1) ) ) ; % prediction of the 1st image
net.meta.classes.description{sorted_idx(end-4:end)}
