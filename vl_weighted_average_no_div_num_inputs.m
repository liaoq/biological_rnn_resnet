function [outputs derParams] = vl_weighted_average_no_div_num_inputs(inputs,params,derOutputs)
% outputs, derParams, inputs, params are all cells
obj_numInputs = numel(inputs);  
if ~exist('derOutputs','var') || isempty(derOutputs) 
    start = 1;
    while isempty(inputs{start})
        start = start + 1;
    end
    if start > numel(inputs)
        outputs{1} = [];     
        return; 
    else
        outputs{1} = inputs{start} .* params{1}(start) ;
        for k = start+1:obj_numInputs  
            if ~isempty(inputs{k})
                outputs{1} = outputs{1} + inputs{k} .* params{1}(k) ;
            end
        end
        
        %outputs{1} = outputs{1}./obj_numInputs;
        
    end
else
    if  isempty(derOutputs) || isempty(derOutputs{1})
        outputs = cell(size(inputs)); 
        derParams = cell(size(params)); 
    end
    
    %derOutputs{1} = derOutputs{1}./obj_numInputs;
    
    derParams = params{1}.*0;
    for k = 1:obj_numInputs
        if ~isempty(inputs{k})
            derInputs{k} = derOutputs{1} .* params{1}(k); 
            tmpDer = derOutputs{1} .* inputs{k};
            derParams(k) = sum(tmpDer(:));
        end
    end
    derParams = {derParams} ;
    outputs   = derInputs; 
end
