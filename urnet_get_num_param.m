function [numParam decompose] = urnet_get_num_param(net)
% get the number of parameters

param_names = {net.params.name};
param_counted = zeros(numel(param_names),1); 

decompose.BatchNorm = 0;
decompose.Conv = 0; 
decompose.Average = 0;

for i = 1:numel(net.layers)
    current_layer = net.layers(i);
    if isfield(net.layers(i),'type') && ~isempty(net.layers(i).type)
        theType = net.layers(i).type;
    else
        theType = class(current_layer.block); 
    end
    switch theType
      case {'dagnn.Conv','dagnn.ConvTranspose'}  
        for p = 1:2
            idx = find(strcmp(param_names,current_layer.params{p}));
            if param_counted(idx) == 1
                continue; 
            else
                param_counted(idx) = 1; 
            end
            decompose.Conv = decompose.Conv + numel(net.params(idx).value);
        end
      case 'dagnn.Average'
        for p = 1
            idx = find(strcmp(param_names,current_layer.params{p}));
            if net.params(idx).learningRate ~= 0
                if param_counted(idx) == 1
                    continue;
                else
                    param_counted(idx) = 1;
                end                
                decompose.Average = decompose.Average + numel(net.params(idx).value);
            end
        end
      case 'dagnn.BatchNorm'
        if current_layer.block.disableParams == 0
            for p = 1:2
                idx = find(strcmp(param_names,current_layer.params{p}));
                if param_counted(idx) == 1
                    continue;
                else
                    param_counted(idx) = 1; 
                end                
                decompose.BatchNorm = decompose.BatchNorm + numel(net.params(idx).value);
            end
        end
      case 'dagnn.BatchNormOverTime'
        if current_layer.block.disableParams == 0
            for p = 1:2
                idx = find(strcmp(param_names,current_layer.params{p}));
                if current_layer.block.share_over_time(p) == 1
                    disp('warning: if shared, I assume all other layers used this param idx are running the shared mode.');
                    if param_counted(idx) == 1
                        continue;
                    else
                        param_counted(idx) = 1; 
                    end
                    decompose.BatchNorm = decompose.BatchNorm + numel(net.params(idx).value(:,:,:,:,1));
                else
                    decompose.BatchNorm = decompose.BatchNorm + numel(net.params(idx).value(:,:,:,:,1));
                end
            end
        end
      case {'dagnn.ReLU', 'dagnn.Pooling', 'dagnn.Loss'}
        
      otherwise
        error(['type ' theType ' not supported.']);  
    end
end

fields = fieldnames(decompose);
numParam = 0;
for i = 1:numel(fields)
    numParam = numParam + decompose.(fields{i});
end
