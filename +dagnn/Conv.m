classdef Conv < dagnn.Filter
    properties
        size = [0 0 0 0]
        hasBias = true
        opts = {'cuDNN'}
        trainMethod = 'gradient'
        conv_func   = @vl_nnconv
        initMethod
    end

    methods
        function outputs = forward(obj, inputs, params)
            if numel(obj.pad) == 2
                obj.pad = [obj.pad(1) obj.pad(1) obj.pad(2) obj.pad(2)]; 
            end 
            if ~obj.hasBias, params{2} = [] ; end
            %wait(gpuDevice);
            if isempty(obj.conv_func) || strcmp(func2str(obj.conv_func),'vl_nnconv')
                outputs{1} = vl_nnconv(...
                    inputs{1}, params{1}, params{2}, ...
                    'pad', obj.pad, ...
                    'stride', obj.stride, ...
                    obj.opts{:}) ;
            else
                outputs{1} = obj.conv_func(...
                    inputs{1}, params{1}, params{2}, [], ...
                    'pad', obj.pad, ...
                    'stride', obj.stride, ...
                    obj.opts{:}) ;
            end
        end

        function [derInputs, derParams] = backward(obj, inputs, params, derOutputs)
            if numel(obj.pad) == 2
                obj.pad = [obj.pad(1) obj.pad(1) obj.pad(2) obj.pad(2)]; 
            end 
            if ~obj.hasBias, params{2} = [] ; end
            %wait(gpuDevice);
            if isempty(obj.conv_func) || strcmp(func2str(obj.conv_func),'vl_nnconv')
                [derInputs{1}, derParams{1}, derParams{2}] = vl_nnconv(...
                    inputs{1}, params{1}, params{2}, derOutputs{1}, ...
                    'pad', obj.pad, ...
                    'stride', obj.stride, ...
                    obj.opts{:}) ;                
            else
                [derInputs{1}, derParams{1}, derParams{2}] = obj.conv_func(...
                    inputs{1}, params{1}, params{2}, derOutputs{1}, ...
                    'pad', obj.pad, ...
                    'stride', obj.stride, ...
                    obj.opts{:}) ;
                if isempty( derParams{2} )
                    derParams{2} = params{2} * 0;
                end
            end
        end

        function kernelSize = getKernelSize(obj)
            kernelSize = obj.size(1:2) ;
        end

        function outputSizes = getOutputSizes(obj, inputSizes)
            outputSizes = getOutputSizes@dagnn.Filter(obj, inputSizes) ;
            outputSizes{1}(3) = obj.size(4) ;
        end

        function params = initParams(obj)
            sc = sqrt(2 / prod(obj.size(1:3))) ;
            params{1} = randn(obj.size,'single') * sc ;
            if obj.hasBias
                params{2} = zeros(obj.size(4),1,'single') * sc ;
            end
        end

        function set.size(obj, ksize)
        % make sure that ksize has 4 dimensions
            ksize = [ksize(:)' 1 1 1 1] ;
            obj.size = ksize(1:4) ;
        end

        function obj = Conv(varargin)
            obj.load(varargin) ;
            % normalize field by implicitly calling setters defined in
            % dagnn.Filter and here
            obj.size = obj.size ;
            obj.stride = obj.stride ;
            obj.pad = obj.pad ;
        end
        
        function attach(obj, net, index)
            attach@dagnn.Filter(obj, net, index) ;
            % change the update method of filters
            if iscell( obj.trainMethod )
                for iii = 1:2
                    p = net.getParamIndex(net.layers(index).params{iii}) ;
                    net.params(p).trainMethod = obj.trainMethod{iii};
                end
            elseif isstr( obj.trainMethod )
                for iii = 1:min([2 numel(net.layers(index).params)])
                    p = net.getParamIndex(net.layers(index).params{iii}) ;
                    net.params(p).trainMethod = obj.trainMethod;
                end
            elseif isa( obj.trainMethod,'function_handle')
                p = net.getParamIndex(net.layers(index).params{1}) ;
                net.params(p).trainMethod = obj.trainMethod;
            else
                error('not surpported');
            end
        end
        
    end
end
