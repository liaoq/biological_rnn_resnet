classdef Average < dagnn.ElementWise
%AVERAGE DagNN average layer
%   The AVERAGE layer takes the average of all its inputs and store the result
%   as its only output.
    
    properties (Transient)
        numInputs
        %div_num_inputs = false;
        average_func = @vl_weighted_average_no_div_num_inputs
        misc 
    end

    methods
        function outputs = forward(obj, inputs, params)
            outputs = obj.average_func(inputs,params);     
        end

        function [derInputs, derParams] = backward(obj, inputs, params, derOutputs)
            [derInputs derParams] = obj.average_func(inputs,params,derOutputs);   
            % if isempty(derParams{1})
            %     if rand > 0.95
            %         disp(['Warning: Average module ' obj.misc.name ' has empty der. making it 0']);
            %     end 
            %     assert(isempty(derOutputs{1}));    
            %     derParams{1} = params{1}*0;  
            % end 
        end

        function outputSizes = getOutputSizes(obj, inputSizes)
            outputSizes{1} = inputSizes{1} ;
            for k = 2:numel(inputSizes)
                if all(~isnan(inputSizes{k})) && all(~isnan(outputSizes{1}))
                    if ~isequal(inputSizes{k}, outputSizes{1})
                        warning('Average layer: the dimensions of the input variables is not the same.') ;
                    end
                end
            end
        end

        function rfs = getReceptiveFields(obj)
            numInputs = numel(obj.net.layers(obj.layerIndex).inputs) ;
            rfs.size = [1 1] ;
            rfs.stride = [1 1] ;
            rfs.offset = [1 1] ;
            rfs = repmat(rfs, numInputs, 1) ;
        end

        function obj = Average(varargin)
            obj.load(varargin) ;
        end
        
        function attach(obj, net, index)
            attach@dagnn.ElementWise(obj, net, index) ; 
            for iii = 1:numel(net.layers(index).params)
                p = net.getParamIndex(net.layers(index).params{iii}) ;
                net.params(p).misc.type = 'Average';
            end 
        end
        
    end
end

