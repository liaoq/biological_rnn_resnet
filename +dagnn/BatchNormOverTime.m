classdef BatchNormOverTime < dagnn.ElementWise
    properties
        % qianli added
        disableParams = false;
        bnorm_func = @vl_nnbnorm;
        numChannels
        epsilon = 1.0000e-05
        share_over_time = [0 0 0]
        current_time
        param_names
        misc
    end
    
    methods
        function outputs = forward(obj, inputs, params)
            
        % qianli added
            if ~strcmp(func2str(obj.bnorm_func),'vl_nnbnorm') % ~isempty(fieldnames(obj.misc)) 
                
                first_param_idx = obj.net.getParamIndex( obj.param_names{1}); 
                param_misc = obj.net.params(first_param_idx).misc;
                
                param_misc.mode = obj.net.mode;
                [param_misc outputs] = obj.bnorm_func(param_misc, inputs, params) ;
                
                obj.net.params(first_param_idx).misc = param_misc;
                
            else 
                if obj.disableParams
                    params{1} = params{1}*0 + 1;
                    params{2} = params{2}*0;
                end
                % original BN
                for ii = 1:3
                    if ii == 3 && obj.current_time > size(params{ii},5)  
                        last_used_idx = gather(find( params{end} >= 1, 1, 'last'));   
                        p_idx(ii) = last_used_idx;  % largest, but not larger than max_t 
                    else 
                        p_idx(ii) = obj.current_time;   
                    end
                end   
                for ii = 1:3
                    if obj.share_over_time(ii)
                        p_idx(ii) = 1;
                    end
                end
                for ii = 1:3
                    p_{ii} = params{ii}(:,:,:,:,p_idx(ii));
                end
                if strcmp(obj.net.mode, 'test') || (numel(obj.share_over_time) == 4 && obj.share_over_time(4)==999 )
                    outputs{1} = obj.bnorm_func(inputs{1}, p_{1}, p_{2}, 'moments', p_{3}) ;
                else
                    outputs{1} = obj.bnorm_func(inputs{1}, p_{1}, p_{2}) ;
                end
            end
        end

        function [derInputs, derParams] = backward(obj, inputs, params, derOutputs)
            
            if ~strcmp(func2str(obj.bnorm_func),'vl_nnbnorm') % ~isempty(fieldnames(obj.misc))  
                
                first_param_idx = obj.net.getParamIndex( obj.param_names{1});  
                param_misc = obj.net.params(first_param_idx).misc;
                
                param_misc.mode = obj.net.mode;
                [param_misc, derInputs, derParams] = obj.bnorm_func(param_misc, inputs, params, derOutputs) ;
                
                obj.net.params(first_param_idx).misc = param_misc;
                %derParams{3} = derParams{3} * size(inputs{1},4) ;            
            else
                
                last_used_idx = gather(find( params{end} >= 1, 1, 'last'));  
                for ii = 1:3
                    if ii == 3 && obj.current_time > size(params{ii},5)  
                        p_idx(ii) = last_used_idx;  % largest, but not larger than max_t 
                    else 
                        p_idx(ii) = obj.current_time;  
                    end
                end
                for ii = 1:3
                    if obj.share_over_time(ii) 
                        p_idx(ii) = 1;
                    end
                end
                for ii = 1:3
                    p_{ii} = params{ii}(:,:,:,:,p_idx(ii));
                end 
                if  ( numel(obj.share_over_time) == 4 && obj.share_over_time(4)==999 )  
                    error('not used any more');
                    [derInputs{1}, der_p_{1}, der_p_{2}, der_p_{3}] = ...
                        obj.bnorm_func(inputs{1}, p_{1}, p_{2}, derOutputs{1},'moments',p_{3}) ; 
                else
                    [derInputs{1}, der_p_{1}, der_p_{2}, der_p_{3}] = ...
                        obj.bnorm_func(inputs{1}, p_{1}, p_{2}, derOutputs{1}) ; 
                end
                % multiply the moments update by the number of images in the batch
                % this is required to make the update additive for subbatches
                % and will eventually be normalized away
                
                for ii = 1:3
                    derParams{ii} = params{ii}*0;
                    derParams{ii}(:,:,:,:,p_idx(ii)) = der_p_{ii};
                end
                
                if obj.current_time > numel( params{4} ) 
                    shared_num_to_div =  params{4}(last_used_idx); 
                else
                    shared_num_to_div =  params{4}(obj.current_time);
                end
                
                if obj.share_over_time(3) == 1 
                    first_param_idx = obj.net.getParamIndex( obj.param_names{1}); 
                    param_misc = obj.net.params(first_param_idx).misc;
                    derParams{3} = derParams{3} ./ numel(param_misc.shared_by); % divide by # shared by
                else
                    if isempty(shared_num_to_div)
                        if rand > 0.9
                            disp('shared_num_to_div is empty. Is it okay????');
                        end
                        shared_num_to_div = 1; 
                    end
                    %derParams{3} = derParams{3} * size(inputs{1},4) ./ shared_num_to_div;
                    derParams{3} = derParams{3} ./ shared_num_to_div;
                    %derParams{3} = derParams{3} * size(inputs{1},4); % ./ shared_num_to_div;
                end
                
                % qianli added
                if obj.disableParams
                    derParams{1} = derParams{1}*0;
                    derParams{2} = derParams{2}*0;
                end

                for ii = 4:numel(params)
                    derParams{ii} = []; % params{ii}*0;  
                end
                
            end
            
        end

        % ---------------------------------------------------------------------
        function obj = BatchNormOverTime(varargin)
            obj.load(varargin{:}) ;
        end

        function attach(obj, net, index)
            attach@dagnn.ElementWise(obj, net, index) ;
            p = net.getParamIndex(net.layers(index).params{3}) ;
            %net.params(p).trainMethod = 'average_my' ;   
            net.params(p).trainMethod = 'average_my_no_div_batchsize' ;
            
            p = net.getParamIndex(net.layers(index).params{4}) ;
            net.params(p).trainMethod = 'bnormovertime_times_dummy' ;     
            for iii = 1:numel(net.layers(index).params)
                p = net.getParamIndex(net.layers(index).params{iii}) ;
                net.params(p).misc.type = 'BatchNormOverTime';
            end     
        end
    end
end
