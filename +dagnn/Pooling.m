classdef Pooling < dagnn.Filter
    properties
        method = 'max'
        poolSize = [1 1]
        opts = {'cuDNN'}
    end

    methods
        function outputs = forward(self, inputs, params)
            poolSize = self.poolSize;
            for pp = 1:2
                if isinf(poolSize(pp))
                    poolSize(pp) = size(inputs{1},pp);
                end
            end
            outputs{1} = vl_nnpool(inputs{1}, poolSize, ...
                                   'pad', self.pad, ...
                                   'stride', self.stride, ...
                                   'method', self.method, ...
                                   self.opts{:}) ;
        end

        function [derInputs, derParams] = backward(self, inputs, params, derOutputs)
            poolSize = self.poolSize;
            for pp = 1:2
                if isinf(poolSize(pp))
                    poolSize(pp) = size(inputs{1},pp);
                end
            end
            derInputs{1} = vl_nnpool(inputs{1}, poolSize, derOutputs{1}, ...
                                     'pad', self.pad, ...
                                     'stride', self.stride, ...
                                     'method', self.method, ...
                                     self.opts{:}) ;
            derParams = {} ;
        end

        function kernelSize = getKernelSize(obj)
            kernelSize = obj.poolSize ;
        end

        function outputSizes = getOutputSizes(obj, inputSizes)
            outputSizes = getOutputSizes@dagnn.Filter(obj, inputSizes) ;
            outputSizes{1}(3) = inputSizes{1}(3) ;
        end

        function obj = Pooling(varargin)
            obj.load(varargin) ;
        end
    end
end
