classdef BatchNorm < dagnn.ElementWise
    properties
        % qianli added
        disableParams = false;
        bnorm_func = @vl_nnbnorm;
        numChannels
        epsilon = 1.0000e-05
        param_names
        misc
    end
    
    methods
        function outputs = forward(obj, inputs, params)
        % qianli added
            if ~strcmp(func2str(obj.bnorm_func),'vl_nnbnorm') % ~isempty(fieldnames(obj.misc)) 
                
                first_param_idx = obj.net.getParamIndex( obj.param_names{1}); 
                param_misc = obj.net.params(first_param_idx).misc;
                
                param_misc.mode = obj.net.mode;
                [param_misc outputs] = obj.bnorm_func(param_misc, inputs, params) ;
                
                obj.net.params(first_param_idx).misc = param_misc;
                 
            else
                if obj.disableParams
                    params{1} = params{1}*0 + 1;
                    params{2} = params{2}*0;
                end                
                if strcmp(obj.net.mode, 'test')
                    outputs{1} = obj.bnorm_func(inputs{1}, params{1}, params{2}, 'moments', params{3}) ; 
                else
                    outputs{1} = obj.bnorm_func(inputs{1}, params{1}, params{2}) ;
                end
            end
        end

        function [derInputs, derParams] = backward(obj, inputs, params, derOutputs)
            if ~strcmp(func2str(obj.bnorm_func),'vl_nnbnorm') % ~isempty(fieldnames(obj.misc))  
                
                first_param_idx = obj.net.getParamIndex( obj.param_names{1});  
                param_misc = obj.net.params(first_param_idx).misc;
                
                param_misc.mode = obj.net.mode;
                [param_misc, derInputs, derParams] = obj.bnorm_func(param_misc, inputs, params, derOutputs) ;
                
                obj.net.params(first_param_idx).misc = param_misc;
                %derParams{3} = derParams{3} * size(inputs{1},4) ;
                
            else
                [derInputs{1}, derParams{1}, derParams{2}, derParams{3}] = ...
                    obj.bnorm_func(inputs{1}, params{1}, params{2}, derOutputs{1}) ; 
                % multiply the moments update by the number of images in the batch
                % this is required to make the update additive for subbatches
                % and will eventually be normalized away
                derParams{3} = derParams{3} * size(inputs{1},4) ;
                
                % qianli added
                if obj.disableParams
                    derParams{1} = derParams{1}*0;
                    derParams{2} = derParams{2}*0;
                end  
            end
            
        end

        % ---------------------------------------------------------------------
        function obj = BatchNorm(varargin)
            obj.load(varargin{:}) ;
        end

        function attach(obj, net, index)
            attach@dagnn.ElementWise(obj, net, index) ;
            p = net.getParamIndex(net.layers(index).params{3}) ;
            net.params(p).trainMethod = 'average_my' ;
            for iii = 1:numel(net.layers(index).params)
                p = net.getParamIndex(net.layers(index).params{iii}) ;
                net.params(p).misc.type = 'BatchNorm';
            end 
        end
    end
end
